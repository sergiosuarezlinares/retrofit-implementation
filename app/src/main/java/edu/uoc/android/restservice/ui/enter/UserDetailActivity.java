package edu.uoc.android.restservice.ui.enter;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Owner;
import edu.uoc.android.restservice.rest.service.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDetailActivity extends AppCompatActivity {

	//Statics
	private static final String EXTRA_TEXT = "bundleText";
	//Vars
	private Activity activity = this;
	private RecyclerView mRecyclerView;
	private FollowerAdapter mAdapter;

	private ImageView imageView;
	private ProgressBar progressBar;
	private RelativeLayout userProfile;
	private TextView textViewError;
	private TextView textViewRepositories;
	private TextView textViewFollowing;
	private TextView textViewFollowers;
	private String mString;

	public static void startActivity(Context context, String text) {
		Intent intent = new Intent(context, UserDetailActivity.class);
		intent.putExtra(EXTRA_TEXT, text);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_detail);
		initView();
		retrieveData();
	}

	public void initView() {
		//String to search for
		mString = getIntent().getExtras().getString(EXTRA_TEXT);

		//Toolbar
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		//View elements
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		userProfile = (RelativeLayout) findViewById(R.id.user_profile);
		textViewError = (TextView) findViewById(R.id.empty_view);
		imageView = (ImageView) findViewById(R.id.image_view);
		textViewRepositories = (TextView) findViewById(R.id.text_view_repositories);
		textViewFollowing = (TextView) findViewById(R.id.text_view_following);
		textViewFollowers = (TextView) findViewById(R.id.text_view_followers);
		mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

		//Text image
		textViewFollowers.setText(R.string.activity_textview_followers);
		textViewError.setText(R.string.activity_textview_error);

		//Adapter
		mAdapter = new FollowerAdapter(this);

		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
		mRecyclerView.setLayoutManager(mLayoutManager);
		mRecyclerView.setAdapter(mAdapter);
	}

	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return true;
	}

	public void retrieveData() {
		Call<Owner> callOwner = new GitHubAdapter().getOwner(mString);
		callOwner.enqueue(new Callback<Owner>() {
			@Override
			public void onResponse(Call<Owner> call, Response<Owner> response) {
				Owner owner = response.body();
				if (owner != null) {
					if (owner.getAvatarUrl() != null) {
						Picasso.with(activity).load(owner.getAvatarUrl()).into(imageView);
					}
					if (owner.getPublicRepos() != null) {
						textViewRepositories.setText(getString(R.string.activity_textview_repositories) + " : " + Integer.toString(owner.getPublicRepos()));
					}
					if (owner.getFollowing() != null) {
						textViewFollowing.setText(getString(R.string.activity_textview_following) + " : " + Integer.toString(owner.getFollowing()));
					}
					updateUI(true);
				} else {
					updateUI(false);
				}

			}

			@Override
			public void onFailure(Call<Owner> call, Throwable t) {
				updateUI(false);
			}
		});

		Call<List<Owner>> callFollowers = new GitHubAdapter().getFollowers(mString);
		callFollowers.enqueue(new Callback<List<Owner>>() {
			@Override
			public void onResponse(Call<List<Owner>> call, Response<List<Owner>> response) {
				List<Owner> list = response.body();
				if (list != null && list.size() > 0) {
					//Update adapter list
					mAdapter.setFollowersList(list);
					mAdapter.notifyDataSetChanged();
					updateUI(true);
				} else {
					updateUI(false);
				}
			}

			@Override
			public void onFailure(Call<List<Owner>> call, Throwable t) {
				updateUI(false);
			}
		});
	}

	private void updateUI(boolean successDownloading) {
		if (successDownloading) {
			progressBar.setVisibility(View.GONE);
			textViewError.setVisibility(View.GONE);
			userProfile.setVisibility(View.VISIBLE);
		} else {
			progressBar.setVisibility(View.GONE);
			userProfile.setVisibility(View.GONE);
			textViewError.setVisibility(View.VISIBLE);
		}
	}

}
