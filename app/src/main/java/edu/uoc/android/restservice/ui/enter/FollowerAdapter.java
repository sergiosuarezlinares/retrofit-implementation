package edu.uoc.android.restservice.ui.enter;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Owner;

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.FollowerViewHolder> {

	private Context context;
	private List<Owner> followersList;

	public FollowerAdapter(Context context) {
		this.context = context;
	}

	public void setFollowersList(List<Owner> list) {
		this.followersList = list;
	}

	@Override
	public FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new FollowerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_follower, parent, false));
	}

	@Override
	public void onBindViewHolder(FollowerViewHolder holder, int position) {
		Owner follower = followersList.get(position);
		if (follower.getAvatarUrl() != null) {
			Picasso.with(context).load(follower.getAvatarUrl()).into(holder.imageView);
		}
		if (follower.getLogin() != null) {
			holder.textView.setText(follower.getLogin());
		}
	}

	@Override
	public int getItemCount() {
		return followersList != null ? followersList.size() : 0;
	}

	public class FollowerViewHolder extends RecyclerView.ViewHolder {

		public ImageView imageView;
		public TextView textView;

		public FollowerViewHolder(View view) {
			super(view);
			imageView = (ImageView) view.findViewById(R.id.image_view);
			textView = (TextView) view.findViewById(R.id.text_view_name);
		}

	}

}
